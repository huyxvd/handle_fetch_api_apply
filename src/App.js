import React from 'react'
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom'
import Edit from './Components/Edit'
import Home from './Components/Home'
import Login from './Components/Login'
import Post from './Components/Post'
import PriveRoute from './Components/Priviterouter'



export default function App() {
  return (
    <>
      <BrowserRouter>
        <Link to={"/"}>Home</Link>
        <Link to={"/"}></Link>
        <Link to={"/Edit"}></Link>
        <Routes>
          <Route path='/' element={
            // privetrouter (logic có tài khoản ở localstorage mới cho render component Home, không có điều hướng sang Login)
            <PriveRoute>
              <Home />
            </PriveRoute>} />

          <Route path='/Login' element={<Login />} />
          <Route path='/Post' element={<Post />} />
          <Route path='/Edit' element={<Edit />} />
        </Routes>

      </BrowserRouter>
    </>
  )
}
