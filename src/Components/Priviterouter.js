import React, { Component } from 'react'
import { Navigate } from 'react-router-dom'


function PriveRoute(props) {
    return (
        JSON.parse(localStorage.getItem("User")) ?
            props.children
            :
            <Navigate to={'/Login'} />
    )
}

export default PriveRoute
