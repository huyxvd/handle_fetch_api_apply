import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'

export default function Home() {
  // state undifined để handle toán tử && trong render
  const [list, setList] = useState(undefined)
  const [email, setEmail] = useState("") //handle cách 2

  // navigate
  const nav = useNavigate()

  // fetch data user 1 lần duy nhất
  useEffect(() => {
    fetch('https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/user/user')
      .then(res => res.json())
      .then(dt => setList(dt))
  }, [])
  // handle delete and setList 
  const handleOnclickDelete = (emailraw) => {
    fetch('https://vietcpq.name.vn/U2FsdGVkX19vV1e+G2Dt1h63IVituNJD+GdHSpis9+rOtKy+FbHJqg==/user/user', {
      method: 'DELETE',
      headers: {
        'Accept': "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ "Email": emailraw })
    })
      .then(() => setList(prev => prev.filter((e) => {
        return e.Email != emailraw
      })))
  }

  // logout handle xoá localstorage
  const HandleLogout = () => {
    localStorage.removeItem("User");
    nav('/Login')
  }

  // chuyển trang và truyền dữ liệu cho Component Edit
  const handleOnclickChange = (u) => {
    nav('/Edit', {
      state: {
        user: u
      }
    })
  }
  return (
    <>
      <Link to={'/Post'}>--------!---------Post</Link>
      {list &&
        <div>
          <input placeholder='Email' />
          <input placeholder='Name' />
          <input placeholder='Role' />
          <button>UPDATE</button>
          <table border={1} style={{ borderCollapse: "collapse" }}>
            <thead>
              <tr>
                <td>Email</td>
                <td>Name</td>
                <td>Role</td>
              </tr>
            </thead>
            <tbody>
              {list.map((e) => {
                return <tr key={e.Email}>
                  <th>{e.Email}</th>
                  <th>{e.Name}</th>
                  <th>{e.Role}</th>
                  {/* delete */}
                  <th onClick={() => handleOnclickDelete(e.Email)}>Delete</th>
                  {/* change */}
                  <th onClick={() => handleOnclickChange(e)}>handleOnclickChange</th>
                </tr>

              })}
            </tbody>
          </table>
          <hr></hr>
          {/* logout */}
          <button onClick={HandleLogout}>logout</button>

        </div>}
    </>
  )
}
